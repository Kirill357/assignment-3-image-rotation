#include "bmp.h"
#define BMP_SIGNATURE 0x4d42
#define BI_SIZE 40
#define BF_NO_RES 0
#define BI_PLAN 1
#define BI_BIT_COUNT 24
#define NO_BI_COMPR 0
#define ALL_COLORS 0

enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header header;

    if (!fread(&header, sizeof(struct bmp_header), 1, in)) {
        return READ_INVALID_HEADER;
    }
    if (header.bfType != BMP_SIGNATURE) {
        return READ_INVALID_SIGNATURE;
    }
    uint64_t width = header.biWidth;
    uint64_t height = header.biHeight;
    uint64_t data_size = header.biSizeImage;
    char* bmp_data = (char*) malloc(data_size);
    assert(bmp_data != NULL);
    if (fread(bmp_data, data_size, 1, in) != 1) {
        free(bmp_data);
        return READ_INVALID_BITS;
    }

    *img = image_create(width, height, bmp_data);
    free(bmp_data);

    return READ_OK;
}


enum write_status to_bmp( FILE* out, struct image const* img ) 
{
    struct bmp_header b_head;
    uint16_t os = (4 - (sizeof(struct pixel) * img->width) % 4) % 4;
    uint64_t imgSize = img->height * (img->width * sizeof(struct pixel)  + os);
    b_head.bfType = BMP_SIGNATURE;
    b_head.bfReserved = BF_NO_RES;
    b_head.bOffBits = sizeof(struct bmp_header);
    b_head.biSize = BI_SIZE;
    b_head.biWidth = img->width;
    b_head.biHeight = img->height;
    b_head.biPlanes = BI_PLAN;
    b_head.biBitCount = BI_BIT_COUNT;
    b_head.biCompression = NO_BI_COMPR;
    b_head.biSizeImage = imgSize;
    b_head.bfileSize = imgSize + b_head.bOffBits;
    b_head.biXPelsPerMeter = 1;
    b_head.biYPelsPerMeter = 1;
    b_head.biClrUsed = ALL_COLORS;
    b_head.biClrImportant = ALL_COLORS;
    

    if (!fwrite(&b_head, sizeof(struct bmp_header), 1, out)) {
        return WRITE_ERROR;
    }
    
    for (size_t i = 0; i < img->height; i++) {
        if (fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out) != img->width) {
            return WRITE_ERROR;
        }

        for (uint16_t p = 0; p < os; p++) {
            if (fputc(0, out) == EOF) {
                return WRITE_ERROR;
            }
        }
    }
    return WRITE_OK;
}

