#include "rotate.h"

struct image rotate( struct image* const img ) { 
    uint64_t height = img->height;
    uint64_t width = img->width;
    struct image img2 = blank_image(height, width);
    assert(img2.data != NULL);
    for (uint64_t y = 0; y < height; y++) {
        for (uint64_t x = 0; x < width; x++) {
            uint64_t new_x = width - 1 - x;
            img2.data[y + new_x * height] = img->data[y * width + x]; 
        }
    }
    free(img->data);
    return img2;
}

