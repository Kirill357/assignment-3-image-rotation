#include "bmp.h"
#include "image.h"
#include "main.h"
#include "rotate.h"

void rotate_img(long angle, struct image *img) {
    while (angle > 0) {
        *img = rotate(img);
        angle -= 90;
    } 
}

int main( int argc, char** argv ) {
    if (argc != ARG_COUNT) {
        printf("Invalid number of arguments");
        return INVALID_NUM_ARG;
    }
    struct image* img = malloc(sizeof(struct image));
    assert(img != NULL);
    FILE* in = fopen(argv[1], "r");
    enum read_status s = from_bmp(in, img);
    fclose(in);
    if (s != 0) {
        free(img);
        printf("Error reading input file");
        return ERROR_READING;
    }
    char *end;
    long angle = strtol(argv[3], &end, 10);
    angle = (angle + 360) % 360;
    if (angle % 90 == 0 && angle <= 270 && angle >= 0) {
        rotate_img(angle, img);
    }
    else {
        printf("Invalid rotation angle");
        return INVALID_ROTATION;
    }

    
    FILE* out = fopen(argv[2], "w");
    enum write_status wr = to_bmp(out, img);
    if (fclose(out) != 0) {
        return ERROR_WRITING;
    }
    if (wr != 0) {
        free(img->data);
        printf("Error writing to file");
        return ERROR_WRITING;
    }

    free(img->data);
    free(img);
    
    return 0;
}
