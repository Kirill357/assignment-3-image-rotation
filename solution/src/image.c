#include "image.h"

struct image blank_image(uint64_t const width, uint64_t const heigth)
{
    struct image img2;
    img2.height = heigth;
    img2.width = width;
    img2.data = (struct pixel*) malloc(sizeof(struct pixel) * width * heigth);
    return img2;
}

struct image image_create(uint64_t const width, uint64_t const height, char* bmp_data) {
    struct image img2 = blank_image(width, height);
    assert(img2.data != NULL);
    uint64_t kor = (4 - (sizeof(struct pixel) * width) % 4) % 4;
    for (uint64_t i = 0; i < height; i++) {
        for (uint64_t j = 0; j < width; j++) {
            img2.data[i * width + j] = (struct pixel) {bmp_data[0], bmp_data[1], bmp_data[2]};
            bmp_data += sizeof(struct pixel);
        }
        bmp_data += kor;
    }
    return img2;
}

