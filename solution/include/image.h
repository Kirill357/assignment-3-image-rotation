#include<assert.h>
#include<stdint.h>
#include<stdio.h>
#include<stdlib.h>


#ifndef IMAGE_H
#define IMAGE_H


struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct image image_create(uint64_t const width, uint64_t const height, char* bmp_data);
struct image blank_image(uint64_t const width, uint64_t const heigth);

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

#endif
